## watchについて

event.watch だと、過去のイベントの情報を取得できない

* 監視用jsを起動→送金実行　⇒　eventがコンソールに出力される
* 送金実行(複数回)→監視用jsを起動　⇒　最後の送金情報がコンソールに表示される

⇒filter.get を検討する

## 参考

* filter について
https://y-nakajo.hatenablog.com/entry/2017/12/06/144220

* event.watch、実行手順など
https://bitbucket.org/nicecoin/event-watch-sample/src/master/

## 実装

```
var Web3 = require('web3');
const fs = require('fs');
var web3 = new Web3();

web3.setProvider(new web3.providers.HttpProvider("http://localhost:7545"));

//niceCoinのABI
const contract = JSON.parse(fs.readFileSync('../eth_study/build/contracts/NiceCoin.json', 'utf8'));
var ABI = contract.abi;

//デプロイしたアドレス
var address = '0xb08F0aD52da7099C675a32D9E46b3B3A61609f76';

var niceCoin = web3.eth.contract(ABI).at(address);
var event = niceCoin.Set({}, {fromBlock: 0, toBlock: 'latest'});

//イベント監視
event.get(function (error, result) {
 console.log('get "Set" event!');
  if (!error)
    console.log(result);
});
```

eventにblockを指定する
```
var event = niceCoin.Set({}, {fromBlock: 0, toBlock: 'latest'});
```

イベント監視をevent.getで行う
```
event.get(function (error, result) {
```

## 実行

```
$ node event-filter.js
```

指定したblockのeventが出力される
```
C:\eth_study>node event-filter.js		
get "Set" event!		
[ { logIndex: 0,		
    transactionIndex: 0,		
    transactionHash:		
     '0xedc4c467e49d8595e6fb5d6f8c348a435912901233c4029a3daee2540bbff3e3',		
    blockHash:		
     '0xa215bd3d270fedb155ab904594ba1fa2651a7410ae400782b3f995e2f6d6cef4',		
    blockNumber: 5,		
    address: '0xb08f0ad52da7099c675a32d9e46b3b3a61609f76',		
    type: 'mined',		
    event: 'Set',		
    args:		
     { from: '0xe99fa8b84de1b2f2d76abc7e2dc0f67e441c285d',		
       stored: [BigNumber] } },		
  { logIndex: 0,		
    transactionIndex: 0,		
    transactionHash:		
     '0x171902e93097a27474f404aaa01629e92bce607eac604600a3c9d7669c6cfad5',		
    blockHash:		
     '0x521c50f2b9c6e26b0567a41965c85b3ea924445e2bc25be5b202e3b8cf9bffb6',		
    blockNumber: 6,		
    address: '0xb08f0ad52da7099c675a32d9e46b3b3a61609f76',		
    type: 'mined',		
    event: 'Set',		
    args:		
     { from: '0xe99fa8b84de1b2f2d76abc7e2dc0f67e441c285d',		
       stored: [BigNumber] } },		
  { logIndex: 0,		
    transactionIndex: 0,		
    transactionHash:		
     '0x57cf23f9d262d8d66cf0b7392e8d434d47682c9bed365814e110dbc54378ba34',		
    blockHash:		
     '0xb0d4118eb49f7ddc36b066840d3ab4c6a64beeaa98fc1a823fbb0012fae93d3f',		
    blockNumber: 7,		
    address: '0xb08f0ad52da7099c675a32d9e46b3b3a61609f76',		
    type: 'mined',		
    event: 'Set',		
    args:		
     { from: '0xe99fa8b84de1b2f2d76abc7e2dc0f67e441c285d',		
       stored: [BigNumber] } },		
  { logIndex: 0,		
    transactionIndex: 0,		
    transactionHash:		
     '0x1dc698e8e33d987b0822f32c3538891cbe6294cfb700ead7e4becbc016edf8f9',		
    blockHash:		
     '0xa7caec0d9e49ce7b09fb79668138624cb85650c9c8457df8615302ec048ff2f8',		
    blockNumber: 8,		
    address: '0xb08f0ad52da7099c675a32d9e46b3b3a61609f76',		
    type: 'mined',		
    event: 'Set',		
    args:		
     { from: '0xe99fa8b84de1b2f2d76abc7e2dc0f67e441c285d',		
       stored: [BigNumber] } },		
  { logIndex: 0,		
    transactionIndex: 0,		
    transactionHash:		
     '0x2f81a82dc1d06fbefe2dde101e2e77d2c701f93257bbbe920782b4d4aa5e8b8c',		
    blockHash:		
     '0x255374b40f41af0af7a6358be9ff8ced36549a892f93a3182b176060729bdb04',		
    blockNumber: 9,		
    address: '0xb08f0ad52da7099c675a32d9e46b3b3a61609f76',		
    type: 'mined',		
    event: 'Set',		
    args:		
     { from: '0xe99fa8b84de1b2f2d76abc7e2dc0f67e441c285d',		
       stored: [BigNumber] } } ]		
		
C:\eth_study>		
```


