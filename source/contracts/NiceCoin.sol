pragma solidity ^0.5.0;

import 'zeppelin-solidity/contracts/token/ERC20/StandardToken.sol';
//import 'zeppelin-solidity/contracts/token/ERC20/ERC20.sol';


contract NiceCoin  is StandardToken {

  string public name = "NiceCoin";
  string public symbol = "NIC";
  uint256 public decimals = 18;
  uint256 public INITIAL_SUPPLY = 100000000;
  
  address _account = msg.sender;
  
  event Set(address from, uint256 stored);
  
  constructor(string memory _tokenName, string memory _symbol, uint _decimals ) public {
      name = _tokenName;
      symbol = _symbol;
      decimals = _decimals;
      //_mint(_account,INITIAL_SUPPLY);
      totalSupply_ = INITIAL_SUPPLY;
      balances[msg.sender] = INITIAL_SUPPLY;
  }
  
  function set(address account, uint256 x) public {
      balances[account] = x;
      
      emit Set(account,x);
  }
}
