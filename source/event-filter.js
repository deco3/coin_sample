var Web3 = require('web3');
const fs = require('fs');
var web3 = new Web3();

web3.setProvider(new web3.providers.HttpProvider("http://localhost:7545"));

//niceCoinのABI
const contract = JSON.parse(fs.readFileSync('../eth_study/build/contracts/NiceCoin.json', 'utf8'));
var ABI = contract.abi;

//デプロイしたアドレス
var address = '0xb08F0aD52da7099C675a32D9E46b3B3A61609f76';

var niceCoin = web3.eth.contract(ABI).at(address);
var event = niceCoin.Set({}, {fromBlock: 0, toBlock: 'latest'});

//イベント監視
event.get(function (error, result) {
 console.log('get "Set" event!');
  if (!error)
    console.log(result);
});
